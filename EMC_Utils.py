#!/usr/bin/env python3

import re
import win32clipboard
import pdb

def get_number_list():
    num_regex_match = re.compile('\d+')

    win32clipboard.OpenClipboard()
    raw_num_list = win32clipboard.GetClipboardData()
    win32clipboard.CloseClipboard()

    num_list = num_regex_match.findall(raw_num_list)
    num_list.sort()
    return num_list

def get_duplicate_counts(num_list):
    return {k: num_list.count(k) for k in num_list if num_list.count(k) >= 2}

def create_ranges(numberlist):
    tempdic={}
    returnstring=''

    numberlist = [int(digit) for digit in numberlist]

    for number in numberlist:
        if number-1 in tempdic.keys():
            tempdic[number-1]=number
        elif number-1 in tempdic.values():
            for key in tempdic.keys():
                if number-1==tempdic[key]: foundkey=key
            tempdic[foundkey]=number
        else:
            tempdic[number]=0

    keylist=list(tempdic.keys())
    keylist.sort()

    for key in keylist:
        if tempdic[key]>0:
            returnstring+=(str(key)+'-'+str(tempdic[key])+',')
        else: returnstring+=str(key)+','

    return returnstring[:-1] #Trims off the trailing comma

def send_ranges_to_clipboard(range_string):
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(range_string, win32clipboard.CF_TEXT)
    win32clipboard.CloseClipboard()

def display_range_info():
    num_list = get_number_list()
    duplicate_dict = get_duplicate_counts(num_list)
    range_string = create_ranges(num_list)

    print('Total number of records: ' + str(len(num_list)))

    if duplicate_dict:
        for k,v in duplicate_dict.items():
            print(str(k) + ' occurs ' + str(v) + ' time(s)')

    print('Range:\n' + range_string)

    return range_string

send_ranges_to_clipboard(display_range_info())
input('Press enter to exit.')