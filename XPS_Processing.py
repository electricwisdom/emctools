#!/usr/bin/env python3

import pdb
import zipfile
import re
import itertools
import win32clipboard

EXAMPLE_XPS = zipfile.ZipFile('NewEnvAllBarcodes.xps', 'r')
NO_COLUMN_BREAK = -1234
HEADERS = ['#', 'Barcode', 'Zone/Location',
           'Inheritance Type', 'Master', 'Definition',
           'Price #', 'Alternative Price',
           'Alternate Prep Cost', 'Show Data']

#TODO Iterate beginning of flattened list to determine unique section header sequence
#TODO For x in col_vals_list;if x in HEADERS append; else break
#TODO Parse elements for trailing space and concatenate with next element


def get_fpage_list(xps_doc):
    """returns utf-8 list of strings representing fpage files"""

    fpage_list = []
    for name in xps_doc.namelist():
        if name.endswith('.fpage'):
            fpage_list.append(xps_doc.read(name).decode('utf-8'))

    return fpage_list


def get_all_unicode_strings(fpage_list):
    """returns list of unicode match string lists"""

    uni_reg = re.compile('(?<=UnicodeString\=\").*(?=\")')
    uni_match_string_list = [re.findall(uni_reg,f) for f in fpage_list]

    return uni_match_string_list


def get_all_indices_strings(fpage_list):
    """returns list of indices match string lists"""

    ind_reg = re.compile('(?<=Indices\=\").*(?=\" U)')
    ind_match_string_list = [re.findall(ind_reg,f) for f in fpage_list]

    return ind_match_string_list


def get_int_indices_lists(ind_match_string_list):
    """returns list of indices int tuple lists for each unicodestring in each fpage of the xps doc"""

    ind_string_pairs_list = []

    for string_list in ind_match_string_list:
        indice_strings = [ind_str.split(';') for ind_str in string_list]
        ind_string_pairs_list.append(indice_strings)

    paged_ind_int_tuples = []
    for page in ind_string_pairs_list:
        ind_int_tuples_lists = []
        for string_indices in page:
            ind_int_tuples = []
            for pair_indices in string_indices:
                ind_int_tuples.append(tuple(int(i) for i in pair_indices.split(',')))
            ind_int_tuples_lists.append(ind_int_tuples)
        paged_ind_int_tuples.append(ind_int_tuples_lists)

    return paged_ind_int_tuples


def get_column_break_lists(ind_int_tuples_lists):
    """returns list of column break string indices lists

    Will most likely need to put something in to catch any unicodestrings, that
    will not have any column breaks, such as the beginning one ex:

    Unicode_String = #
    Indices = (6,)

    Possibly append a -1 to indicate no column breaks are present for the current string. """

    paged_col_break_index_lists = []

    for page in ind_int_tuples_lists:
        col_breaks_list = []
        for indice_list in page:
            col_breaks = []
            for i, unicode_char_map in enumerate(indice_list):
                if len(unicode_char_map) == 2 and unicode_char_map[1] >= 100:
                    col_breaks.append(i)
            if col_breaks:
                col_breaks_list.append(col_breaks)
            else:
                col_breaks_list.append(-1234)

        paged_col_break_index_lists.append(col_breaks_list)

    return paged_col_break_index_lists


def get_cell_values_lists(uni_string_lists, col_break_index_lists):
    """returns list of cell value lists

    Needs rewriting to traverse the lists and sublists properly"""

    paged_cell_vals_lists = []

    for uni_list_page,col_list_page in zip(uni_string_lists,col_break_index_lists):
        cell_vals = []
        counter = 0

        while counter < len(uni_list_page):

            if isinstance(col_list_page[counter], list):
                i = 0
                for col_breaks in col_list_page[counter]:
                    cell_vals.append(uni_list_page[counter][i:col_breaks+1])
                    i = col_breaks + 1
                cell_vals.append(uni_list_page[counter][col_breaks+1:])
            else:
                if col_list_page[counter] == -1234:
                    cell_vals.append(uni_list_page[counter])
                else:
                    cell_vals.append(uni_list_page[counter][i:col_breaks+1])
                    i = col_breaks + 1
                    cell_vals.append(uni_list_page[counter][col_breaks+1:])
            counter += 1

        paged_cell_vals_lists.append(cell_vals)

    return paged_cell_vals_lists


def join_headers(fpage):
    """returns an fpage list with the double lined headers joined and the single space fields removed."""

    #Finds every string with a trailing space
    temp_spacey_fields = [x for x in fpage if x[-1] == ' ']
    spacey_fields = []

    #Finds the strings in the temp_spacey_fields that do not begin with a number
    for x in temp_spacey_fields:
        try:
            int(x[0])
            continue
        except:
            spacey_fields.append(x)

    for field in spacey_fields:
        if field == ' ':
            fpage.remove(field)
            continue
        indices = [i for i,x in enumerate(fpage) if x == field]
        for i in indices:
            fpage[i] = fpage[i] + fpage.pop(i + 1)

    return fpage


def test_run():
    fpage_list = get_fpage_list(EXAMPLE_XPS)
    uni_match_string_list = get_all_unicode_strings(fpage_list)
    ind_match_string_list = get_all_indices_strings(fpage_list)
    ind_int_tuples_list = get_int_indices_lists(ind_match_string_list)
    col_break_index_lists = get_column_break_lists(ind_int_tuples_list)
    cell_vals_lists = get_cell_values_lists(uni_match_string_list,col_break_index_lists)
    cell_vals_list = list(itertools.chain.from_iterable(cell_vals_lists))
    pdb.set_trace()
    for p in cell_vals_lists:
        print(p)
    print('Number of fpages: ' + str(len(cell_vals_lists)))
    pdb.set_trace()

if __name__ == "__main__":
    test_run()